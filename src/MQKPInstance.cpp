/*
 * MQKPInstance.cpp
 *
 * Fichero que define los métodos de la clase MQKPInstance. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#include <fstream>
#include <string>
#include "MQKPInstance.h"
#include "MQKPSolution.h"

MQKPInstance::MQKPInstance() {
	//inicializando las variables miembro
	_numKnapsacks = 0;
	_numObjs = 0;
	//Los vectores de la STL se inicializan solos.
}

MQKPInstance::~MQKPInstance() {
	//Los vectores de la STL se liberan solos.
}

int MQKPInstance::getNumKnapsacks() const
{
	return _numKnapsacks;
}

int MQKPInstance::getNumObjs() const
{
	return _numObjs;
}

int MQKPInstance::getProfit(int i, int j) const
{
	return _profits[i][j];
}

int MQKPInstance::getProfit(int i) const
{
	return _profits[i][i];
}

int MQKPInstance::getWeight(int i) const
{
	return _weights[i];
}

int MQKPInstance::getCapacity(int i) const
{
	return _capacities[i];
}

double MQKPInstance::getMaxCapacityViolation(MQKPSolution &solution) {

	double *sumWeights = new double[this->_numKnapsacks + 1];

	for (int j = 1; j <= this->_numKnapsacks; j++) {
		sumWeights[j] = 0;
	}

	for (int i = 0; i < this->_numObjs; i++) {

		/*
		 * 1. Obtener la mochila en la que se encuentra el objeto i-ésimo
		 * 2. Si se encuentra en una mochila válida (mayor que 0), incrementar con el peso del objeto el valor correspondiente en sumWeights.
		 */

		int mochila = solution.whereIsObject(i);

		if(mochila > 0)
		{
			sumWeights[mochila] += _weights[i];
		}

	}

	double maxCapacityViolation = 0; //Inicializamos la máxima violación de alguna mochila a 0, que significa que no hay ninguna violación

	for (int j = 1; j <= this->_numKnapsacks; j++) {

		/*
		 * 1. Calcular la violación en la mochila j-ésima
		 * 2. Actualizar maxCapacityViolation en su caso
		 */

		double violation = sumWeights[j] - _capacities[j];

		if(violation > maxCapacityViolation)
			maxCapacityViolation = violation;
	}

	delete[] sumWeights;
	return maxCapacityViolation;
}

double MQKPInstance::getSumProfits(MQKPSolution &solution) {

	double sumProfits = 0.;

	/*
	 * Doble bucle para cada par de objetos
	 * Tod_o objeto incluido en alguna mochila (> 0) debe sumar su beneficio individual
	 * Tod_o par de objetos incluidos en la misma mochila (y > 0) debe sumar su beneficio conjunto. IMPORTANTE, sumar los pares (i,j) sólo una vez, es decir, si se suma (i, j), no se debe sumar (j, i)
	 */


	for(int i = 0; i < _numObjs -1; i++)
	{
		if(solution.whereIsObject(i) > 0)
		{
			sumProfits += getProfit(i,i); //Añadir el valor del objeto
			for(int j = i+1; j < _numObjs; j++) //Condicion para que no se sumen los profits dos veces
			{
				if(solution.whereIsObject(i) == solution.whereIsObject(j))
				{
					sumProfits += getProfit(i,j);
				}
			}
		}
	}
	//Profit individual del ultimo objeto al que no llega el bucle anterior
	if(solution.whereIsObject(_numObjs - 1) > 0)
	{
		sumProfits += getProfit(_numObjs - 1); 
	}

	return sumProfits;
}

void MQKPInstance::readInstance(char *filename, int numKnapsacks) {

	/*
	 *  completar esta función:
	 *   1. leer el número de objetos
	 *   2. reservar la memoria de vectores y matrices
	 *   3. leer beneficios y pesos de los objetos según lo comentado arriba
	 *   4. Calcular las capacidades de las mochilas:
	 *      . Calcular la suma de los pesos de todos los objetos
	 *      . Multiplicar por 0.8
	 *      . Dividir el resultado anterior entre el número de mochilas. Eso será la capacidad de cada mochila
	 */

	std::ifstream f(filename);

	if(f.is_open())
	{
		std::string line;
		//Leemos el identificador de mochila (que no nos hace falta)
		std::getline(f, line, '\n');

		//Leemos el numero de objetos.
		f >> _numObjs;

		//Reservamos y leemos la matriz de profits
		_profits.resize(_numObjs);

		for(int i = 0; i < _numObjs; i++)
		{
			_profits[i].resize(_numObjs);
		}

		//Leemos el profit de cada objeto individual.
		for(int i = 0; i < _numObjs; i++)
		{
			f >> _profits[i][i];
		}

		//Profit de pares de objetos
		for(int i = 0; i < _numObjs - 1; i++)
		{
			for(int j = i + 1; j < _numObjs; j++)
			{
				int profit;
				f >> profit;
				if(i != j)
				{
					_profits[i][j] = profit;
					_profits[j][i] = _profits[i][j]; //Matriz simetrica
				}
			}
		}

		//Descartamos los valores que no se usan.
		int descartar;
		f >> descartar;
		f >> descartar;


		//Reservamos y leemos el vector de pesos. A la vez calculamos la suma de pesos.
		_weights.resize(_numObjs);

		int sumWeights = 0;

		for(int i = 0; i < _numObjs; i++)
		{
			f >> _weights[i];
			sumWeights += _weights[i];
		}

		//Calculamos la capacidad de las mochilas.
		float capacity = (sumWeights * 0.8) / (float)numKnapsacks;

		//Reservamos el vector de capacidades y lo asignamos.
		_numKnapsacks = numKnapsacks;
		_capacities.resize(1 + numKnapsacks);

		for(int i = 1; i <= numKnapsacks; i++)
		{
			_capacities[i] = capacity;
		}

		f.close();
	}


}

void MQKPInstance::randomPermutation(int size, std::vector<int>& perm) {

	/** TODO
	 * 1. Vacía el vector perm
	 * 2. Llénalo con la permutación identidad
	 * 3. Recórrelo intercambiando cada elemento con otro escogido de forma aleatoria.
	 */
}

double MQKPInstance::getDeltaSumProfits(MQKPSolution& solution, int indexObject,
		int indexKnapsack) {

	double deltaSumProfits = 0;

	/* TODO
	 * Si el objeto estaba en una mochila, resta a deltaSumProfits su beneficio más el beneficio
	 * conjunto con cualquier otro objeto que estuviese en esa misma mochila
	 */

	/* TODO Si el objeto se va a insertar en alguna mochila, suma a deltaSumProfits su beneficio más el beneficio
	 * conjunto con cualquier otro objeto que ya esté en dicha mochila
	 */

	return deltaSumProfits;
}

double MQKPInstance::getDeltaMaxCapacityViolation(MQKPSolution& solution,
		int indexObject, int indexKnapsack) {

	/** TODO
	 * 1. Obten la mochila donde está el objeto
	 * 2. Obten la máxima violación actual de la solución
	 * 3. Asigna el objeto a la nueva mochila en solución
	 * 4. Obten la nueva violación de la solución
	 * 5. Deshaz el cambio anterior, volviendo a poner el objeto en la mochila en la que estaba
	 * 6. Devuelve la diferencia (nueva violación - violación actual)
	 */
}

