/*
 * MQKPSolution.cpp
 *
 * Fichero que define los métodos de la clase MQKPSolution. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#include "MQKPSolution.h"
#include "MQKPInstance.h"

MQKPSolution::MQKPSolution(MQKPInstance &instance) {
	//inicializando las variables miembro. Inicialmente, todos los objetos estarán fuera de las mochilas ( = 0)
	_sol.resize(instance.getNumObjs());
	for(int i = 0; i < instance.getNumObjs(); i++)
	{
		_sol[i] = 0; //Inicializacion a 0
		// 0 = no forma parte de la solucion
	}

	_numObjs = instance.getNumObjs();
	_fitness = 0.0;
}

MQKPSolution::~MQKPSolution() {
}

void MQKPSolution::putObjectIn(int object, int knapsack){
	_sol[object] = knapsack;
}

int MQKPSolution::whereIsObject(int object){
	return _sol[object];
}

double MQKPSolution::getFitness() const {
	return _fitness;
}

void MQKPSolution::setFitness(double fitness) {
	_fitness = fitness;
}

void MQKPSolution::copy(MQKPSolution& solution) {

	/* TODO
	 * 1. Copiar las asignaciones de objetos a mochilas
	 * 2. copiar el fitness
	 */
}
